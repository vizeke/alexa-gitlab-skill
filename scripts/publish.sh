#!/bin/bash

npm run build

7z a alexa.zip package.json node_modules/ build/

aws2 lambda update-function-code --function-name alexaGitlabSkill --zip-file fileb://alexa.zip --publish