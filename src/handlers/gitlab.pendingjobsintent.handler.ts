import { HandlerInput, RequestHandler, getDialogState } from 'ask-sdk-core';
import { Response, IntentRequest, Directive, dialog, er, Slot, slu } from 'ask-sdk-model';
import { GitlabService } from '../services';
import { clearSlotDirective } from './default.handler';

const splitJoin = (name: string, char: string) => {
  return name.indexOf(char) >= 0 ? name.split(char).join(' ') : name;
};

const getSpeechName = (jobName: string) => {
  return splitJoin(splitJoin(jobName, '_'), '-');
};

const pendingJobsHandler = async (): Promise<any> => {
  const gitlabService = new GitlabService();

  const project = await gitlabService.getProject('mutual-life', 'dotnet-api');
  const jobs = await gitlabService.getPendingJobs(project.id);

  let speechText = '';
  if (jobs.length == 0) {
    speechText = 'Nenhum job encontrado';
  } else {
    let plural = '';
    if (jobs.length > 1) {
      plural = 's';
    }

    speechText = `<say-as interpret-as="cardinal">${jobs.length}</say-as> job${plural} encontrado${plural}. `;
    jobs.forEach((job: any, i: number) => {
      speechText += `${getSpeechName(job.name)}: ${job.status}. `;
    });

    speechText += 'Qual deseja executar?';
  }

  return { project, jobs, speechText };
};

export const PendingJobsIntentHandler: RequestHandler = {
  async canHandle(handlerInput: HandlerInput): Promise<boolean> {
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

    console.log('canHandle PendingJobsIntentHandler');
    console.log(sessionAttributes);

    return (
      handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
      handlerInput.requestEnvelope.request.intent.name === 'PendingJobsIntent' &&
      !sessionAttributes.project
    );
  },
  async handle(handlerInput: HandlerInput): Promise<Response> {
    const { project, jobs, speechText } = await pendingJobsHandler();

    if (jobs.length > 0) {
      const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
      sessionAttributes.project = {
        id: project.id,
        name: project.name
      };
      handlerInput.attributesManager.setSessionAttributes(sessionAttributes);
    }

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt('Qual deseja executar?')
      .withSimpleCard('Gitlab', speechText)
      .addDirective(GetUpdateDynamicJobTypeSlotDirective(jobs))
      .getResponse();
  }
};

export const PlayJobIntentHandler: RequestHandler = {
  async canHandle(handlerInput: HandlerInput): Promise<boolean> {
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

    console.log('canHandle PlayJobIntentHandler');

    return (
      handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
      handlerInput.requestEnvelope.request.intent.name === 'PlayJobIntent' &&
      !!sessionAttributes.project
    );
  },
  async handle(handlerInput: HandlerInput): Promise<Response> {
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    const project = sessionAttributes.project;
    let speechText = 'Opção inválida';
    const intentRequest = handlerInput.requestEnvelope.request as IntentRequest;

    if (intentRequest.intent.slots) {
      const value = getFirstSlotValueFromSlot(intentRequest.intent.slots.job);

      console.log({ project, jobSlotValue: value });

      if (!!value) {
        const gitlabService = new GitlabService();
        try {
          const jobId = +value.id;
          await gitlabService.playManualJobs(project.id, jobId);
          speechText = `Executando job ${value.name} do projeto ${getSpeechName(project.name)}`;
        } catch (e) {
          speechText = e.message;
        }
      }
    }

    return handlerInput.responseBuilder
      .speak(speechText)
      .withSimpleCard('Gitlab', speechText)
      .withShouldEndSession(true)
      .addDirective(clearSlotDirective())
      .getResponse();
  }
};

const GetUpdateDynamicJobTypeSlotDirective = (jobs: any[]): dialog.DynamicEntitiesDirective => {
  let updateEntities: dialog.DynamicEntitiesDirective = {
    type: 'Dialog.UpdateDynamicEntities',
    updateBehavior: 'REPLACE',
    types: [
      {
        name: 'jobTypeSlot',
        values: new Array<er.dynamic.Entity>()
      }
    ]
  };

  jobs.forEach(job => {
    if (updateEntities.types) {
      updateEntities.types[0].values.push({
        id: job.id,
        name: {
          value: getSpeechName(job.name),
          synonyms: []
        }
      });
    }
  });

  return updateEntities;
};

type ResolvedValues = {
  authority: string;
  statusCode: slu.entityresolution.StatusCode;
  synonym: string | undefined;
  values: slu.entityresolution.ValueWrapper[];
};

type SlotResult = {
  name: string;
  value?: string;
  resolvedValues: ResolvedValues[];
};

const getStaticAndDynamicSlotValuesFromSlot = (slot: Slot) => {
  const result: SlotResult = {
    name: slot.name,
    value: slot.value,
    resolvedValues: new Array<ResolvedValues>()
  };

  if (slot.resolutions && slot.resolutions.resolutionsPerAuthority && slot.resolutions.resolutionsPerAuthority.length > 0) {
    slot.resolutions.resolutionsPerAuthority.forEach(authority => {
      if (authority.values && authority.values.length > 0) {
        const authorityValue = {
          authority: authority.authority,
          statusCode: authority.status.code,
          synonym: slot.value || undefined,
          values: authority.values
        };

        result.resolvedValues.push(authorityValue);
      }
    });
  }

  return result;
};

const getFirstSlotValueFromSlot = (slot: Slot) => {
  const result = getStaticAndDynamicSlotValuesFromSlot(slot);
  if (result.resolvedValues.length > 0) {
    return result.resolvedValues[0].values[0].value;
  }

  return undefined;
};
