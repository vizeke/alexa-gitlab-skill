import * as dotenv from 'dotenv';
dotenv.config();
import { SkillBuilders } from 'ask-sdk-core';
import {
  LaunchRequestHandler,
  HelpIntentHandler,
  CancelAndStopIntentHandler,
  SessionEndedRequestHandler,
  ErrorRequestHandler,
  PendingJobsIntentHandler,
  PlayJobIntentHandler
} from './handlers';

exports.handler = SkillBuilders.custom()
  .addRequestHandlers(
    LaunchRequestHandler,
    PendingJobsIntentHandler,
    PlayJobIntentHandler,
    HelpIntentHandler,
    CancelAndStopIntentHandler,
    SessionEndedRequestHandler
  )
  .addErrorHandlers(ErrorRequestHandler)
  .lambda();
