import { GitlabApiService } from './gitlab.api.service';
import * as _ from 'lodash';

enum Action {
  play = 'play',
  retry = 'retry',
  cancel = 'cancel'
}

enum JobStatus {
  pending = 'pending',
  started = 'started',
  success = 'success',
  failed = 'failed',
  manual = 'manual',
  canceled = 'canceled'
}

export class GitlabService {
  private api: GitlabApiService;

  constructor() {
    this.api = new GitlabApiService();
  }

  async playManualJobs(projectId: number, jobId: number, action: Action = Action.play): Promise<any> {
    const job: any = await this.api.getProjectJob(projectId, jobId);

    //const selectedJobs = jobs.filter((j: any) => j.stage === stage && j.name === `${stage}_job_${enviroment}`);

    if (!job) {
      console.log('job not found');
      throw new Error('Job não encontrado');
    }

    switch (action) {
      case Action.cancel:
        if (job.status === JobStatus.started) {
          return await this.api.cancelProjectJob(projectId, job.id);
        } else {
          console.log(`Can't cancel a job in status ${job.status}`);
          throw new Error(`Impossível cancelar job no status ${job.status}`);
        }
      default:
        if (job.status === JobStatus.failed) {
          return await this.api.retryProjectJob(projectId, job.id);
        } else if (job.status === JobStatus.manual || job.status == JobStatus.canceled) {
          await this.api.playProjectJob(projectId, job.id);
        } else {
          console.log(`Can't play or retry a job in status ${job.status}`);
          throw new Error(`Impossível executar job no status ${job.status}`);
        }
    }
  }

  async getProject(groupName: string, projectName: string): Promise<any> {
    const groups: any[] = await this.api.getGroups(groupName);
    const projects: any[] = await this.api.getGroupProjects(groups[0].id, projectName);

    return projects[0];
  }

  async getLastPipeline(projectId: number): Promise<any> {
    const pipelines: any[] = await this.api.getProjectPipelines(projectId);
    return pipelines[0];
  }

  async getProjectPendingJobs(projectId: number): Promise<any[]> {
    const jobs = await this.api.getProjectJobs(projectId);
    return jobs.filter(
      (j: any) => j.status === JobStatus.pending || j.status === JobStatus.manual || j.status === JobStatus.started
    );
  }

  async getPipelinePendingJobs(projectId: number, pipelineId: number): Promise<any[]> {
    return await this.api.getProjectPipelineJobs(projectId, pipelineId);
  }

  async getPendingJobs(projectId: number): Promise<any> {
    const pipeline = await this.api.getProjectPipelines(projectId);
    const jobs = await this.api.getProjectPipelineJobs(projectId, pipeline[0].id);

    return _.chain(jobs)
      .orderBy(['id'], ['desc'])
      .uniqBy((job: any) => job.name)
      .filter(this.filterPendingJobs)
      .value();
  }

  private filterPendingJobs = (job: any) => {
    return (
      job.status === JobStatus.pending ||
      job.status === JobStatus.manual ||
      job.status == JobStatus.canceled ||
      job.status == JobStatus.failed
    );
  };
}
