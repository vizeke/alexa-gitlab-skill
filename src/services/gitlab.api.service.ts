import { GitlabConfig } from '../configs';
import fetch, { RequestInit } from 'node-fetch';

export class GitlabApiService {
  private gitlabConfig: GitlabConfig;

  constructor() {
    this.gitlabConfig = new GitlabConfig();
  }

  private fetchJson(url: string, params: any = undefined, options: RequestInit | undefined = undefined): Promise<any> {
    url += `?access_token=${this.gitlabConfig.access_token}`;

    if (params) {
      Object.keys(params).forEach(key => {
        if (params[key]) {
          url += `&${key}=${params[key]}`;
        }
      });
    }

    return fetch(url, options).then(res =>
      res.json().then(json => {
        console.log({ url, json });
        if (!!json.error) {
          throw new Error(`Erro ao executar operação`);
        }
        return json;
      })
    );
  }

  getGroups(search: string = ''): Promise<any> {
    return this.fetchJson(`${this.gitlabConfig.host}/groups`, { search });
  }

  getGroup(id: number): Promise<any> {
    return this.fetchJson(`${this.gitlabConfig.host}/groups/${id}`);
  }

  getGroupProjects(id: number, search: string = ''): Promise<any> {
    return this.fetchJson(`${this.gitlabConfig.host}/groups/${id}/projects`, { search });
  }

  getProjects(search: string = ''): Promise<any> {
    return this.fetchJson(`${this.gitlabConfig.host}/projects`, { search });
  }

  getProject(id: number): Promise<any> {
    return this.fetchJson(`${this.gitlabConfig.host}/projects/${id}`);
  }

  getProjectPipelines(id: number): Promise<any> {
    // Default order by id desc
    return this.fetchJson(`${this.gitlabConfig.host}/projects/${id}/pipelines`);
  }

  getProjectPipeline(projectId: number, id: number): Promise<any> {
    return this.fetchJson(`${this.gitlabConfig.host}/projects/${projectId}/pipelines/${id}`);
  }

  getProjectPipelineJobs(projectId: number, id: number): Promise<any> {
    return this.fetchJson(`${this.gitlabConfig.host}/projects/${projectId}/pipelines/${id}/jobs`);
  }

  getProjectJobs(id: number): Promise<any> {
    return this.fetchJson(`${this.gitlabConfig.host}/projects/${id}/jobs`);
  }

  getProjectJob(projectId: number, id: number): Promise<any> {
    return this.fetchJson(`${this.gitlabConfig.host}/projects/${projectId}/jobs/${id}`);
  }

  playProjectJob(projectId: number, id: number): Promise<any> {
    return this.fetchJson(`${this.gitlabConfig.host}/projects/${projectId}/jobs/${id}/play`, null, { method: 'post' });
  }

  retryProjectJob(projectId: number, id: number): Promise<any> {
    return this.fetchJson(`${this.gitlabConfig.host}/projects/${projectId}/jobs/${id}/retry`, null, { method: 'post' });
  }

  cancelProjectJob(projectId: number, id: number): Promise<any> {
    return this.fetchJson(`${this.gitlabConfig.host}/projects/${projectId}/jobs/${id}/cancel`, null, { method: 'post' });
  }
}
