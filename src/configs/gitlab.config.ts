/**
 * This class have all application config
 */
export class GitlabConfig {
  public get host(): string {
    return this.getEnv('GITLAB_HOST') || 'https://gitlab.com/api/v4';
  }

  public get access_token(): string {
    return this.getEnv('GITLAB_ACCESS_TOKEN') || '123';
  }

  private getEnv(key: string): string | undefined {
    if (!process.env[key]) {
      console.warn(`a variável ${key} não foi definida`);
      return;
    } else {
      return process.env[key];
    }
  }
}
